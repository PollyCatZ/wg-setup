#!/bin/bash

apt update;
apt -y upgrade;
apt install -y sqlite3;
apt install -y python3-pip;
apt-get install -y wireguard;
wget --content-disposition https://gitlab.com/PollyCatZ/wg-setup/-/raw/main/wireguard-install.sh -O wireguard-install.sh && bash wireguard-install.sh;
git clone https://github.com/PollyCatZ/WGManage.git wgdashboard;
cd wgdashboard/src;
chmod u+x wgd.sh;
./wgd.sh install;
chmod -R 755 /etc/wireguard;
./wgd.sh start;
cp wg-dashboard.service /etc/systemd/system/wg-dashboard.service;
chmod 664 /etc/systemd/system/wg-dashboard.service;
systemctl daemon-reload;
sudo systemctl enable wg-dashboard.service;
rm wg-dashboard.service;
wget https://gitlab.com/PollyCatZ/wg-setup/-/raw/main/wg-dashboard.service;
cp wg-dashboard.service /etc/systemd/system/wg-dashboard.service;
chmod 664 /etc/systemd/system/wg-dashboard.service;
systemctl enable wg-dashboard.service;
systemctl start wg-dashboard.service;
ufw allow 51820/udp;
ufw allow 51820/tcp;
ufw allow 10086/tcp;
ufw allow OpenSSH;
ufw disable;
ufw enable;

echo '=========================================================================';
echo  "

Good job!

";	
	
echo '=========================================================================';
